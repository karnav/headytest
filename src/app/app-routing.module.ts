import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {BedroomComponent} from './bedroom/bedroom.component';
import {DetailsComponent} from './details/details.component';
import {DescriptionComponent} from './description/description.component';

const routes: Routes = [
  {path: 'bedroom', component: BedroomComponent},
  // {path: 'details', component: DetailsComponent},
  // {path: 'description', component: DescriptionComponent},
  {path: '', redirectTo: '/bedroom', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
