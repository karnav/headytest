import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-bedroom',
  templateUrl: './bedroom.component.html',
  styleUrls: ['./bedroom.component.scss']
})
export class BedroomComponent implements OnInit {

  selectedfirst = false;
  selectedsecond = false;
  selectedthird = false;
  quantity = 1;
  text = 'DETAILS';
  
  constructor() { }

  ngOnInit() {
    $.fn.followTo = function (pos) {
      const $this = this,
          $window = $(window);
  
      $window.scroll(function (e) {
          if ($window.scrollTop() > pos) {
              $this.css({
                  position: 'absolute',
                  top: pos
              });
          } else {
              $this.css({
                  position: 'fixed',
                  top: 0
              });
          }
      });
  };

  $('#navText').followTo(2400);
  $('#navig').followTo(2400);
  }

  firstButton() {
    this.selectedfirst = true;
    this.selectedsecond = false;
    this.selectedthird = false;
  }

  secondButton() {
    this.selectedfirst = false;
    this.selectedsecond = true;
    this.selectedthird = false;
  }

  thirdButton() {
    this.selectedfirst = false;
    this.selectedsecond = false;
    this.selectedthird = true;
  }

  add() {
    this.quantity += 1;
  }

  sub() {
    this.quantity -= 1;
    if (this.quantity < 0) {
      this.quantity = 0;
    }
  }

  details() {
    this.text = 'DETAILS';
  }

  description() {
    this.text = 'DESCRIPTION';
  }

  reviews() {
    this.text = 'REVIEWS';
  }

  custom() {
    this.text = 'CUSTOM TAB';
  }

}
